import datetime
import calendar

d,m,y = map(int,input().split(' '))

print(calendar.day_name[datetime.date(y,m,d).weekday()].upper())



