# https://www.hackerrank.com/challenges/mini-max-sum

numbers = list(map(int,input().strip().split(' ')))

sums = []

for i in range(5):
    num_temp = numbers.copy()
    del num_temp[i]
    sums.append(sum(num_temp))

print(min(sums),max(sums))