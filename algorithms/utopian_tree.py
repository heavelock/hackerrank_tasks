# https://www.hackerrank.com/challenges/utopian-tree

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    h = 1
    for i in range(n):
        if i%2 == 0:
            h *=2
        else:
            h+=1
    print(h)