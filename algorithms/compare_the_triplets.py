# https://www.hackerrank.com/challenges/compare-the-triplets

a = list(map(int, input().strip().split(' ')))
b = list(map(int, input().strip().split(' ')))

alice = 0
bob = 0
for ai, bi in zip(a, b):
    if ai > bi:
        alice += 1
    if ai < bi:
        bob += 1
print(alice, bob)
