no_cases = int(input().strip())
for ki in range(no_cases):
    nope = False
    no_cubes = int(input().strip())
    cubes = input().split()
    cubes = [ int(f) for f in cubes]

    stack = []

    if cubes[0] >= cubes[-1]:
        stack.append(cubes[0])
        del cubes[0]
    else:
        stack.append(cubes[-1])
        del cubes[-1]

    for j in range(len(cubes)):
        if stack[-1] >= cubes[-1] and cubes[-1] > cubes [0]:
            stack.append(cubes[-1])
            del cubes[-1]
        elif stack[-1] >= cubes[0]:
            stack.append(cubes[0])
            del cubes[0]
        else:
            nope = True

    if nope:
        print('No')
    else:
        print('Yes')






