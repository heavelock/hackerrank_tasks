# This file contains solution to solve a task
# submitten on https://www.hackerrank.com/challenges/collections-counter
# called
# Collection.counter()

import collections

no_shoes = int(input().strip())

shoe_sizes = list(map(int,input().strip().split(' ')))
shoe_sizes = collections.Counter(shoe_sizes)

no_customers = int(input().strip())

prices = []
for i in range(no_customers):
    prices.append(list(map(int,input().strip().split())))

earnings = 0

for shoe in prices:
    if shoe_sizes[shoe[0]] == 0:
        continue
    shoe_sizes[shoe[0]] -= 1
    earnings += shoe[1]

print(earnings)
